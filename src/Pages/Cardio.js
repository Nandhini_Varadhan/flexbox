import React from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {colors} from '../styles/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons
  from 'react-native-vector-icons/MaterialCommunityIcons';

const data = [
  {
    type: 0,
    name: 'Dumbbells',
  },
  {
    type: 1,
    name: 'Mat',
  },
];

const Cardio = () => {
  return (
    <ScrollView style={styles.bgView}>
      <SafeAreaView style={styles.container}>
        <Ionicons
          name="arrow-back-sharp"
          size={22}
          color={colors.CommonColorBB}
          style={{marginTop: 10}}
        />
        <Text style={[styles.heading, {marginTop: 10}]}>Week 1: Day 1</Text>
        <Text style={[styles.title, {marginTop: 15}]}>Cardio Workout</Text>
        <Text style={[styles.txt, {marginTop: 15}]}>
          Do atleast 5-8 rounds. Each round is only
          {'\n'}
          45 Minutes Brounds is only 16 minutes at
          {'\n'}
          active exercise time.
        </Text>
        <Text style={[styles.title, {marginTop: 20}]}>Equipment Needed</Text>

        <FlatList
          data={data}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => (
            <View style={{flex: 0.5, flexDirection: 'row', marginTop: 15}}>
              <View>
                {item.type == 0
                  ? <View style={{flex: 0.7, flexDirection: 'row'}}>
                      <Ionicons
                        name="md-barbell-sharp"
                        size={22}
                        color={colors.orange}
                        style={{marginStart: 6}}
                      />
                    </View>
                  : item.type == 1
                      ? <View style={{flex: 1, flexDirection: 'row'}}>
                          <Ionicons
                            name="ios-tablet-landscape"
                            size={19}
                            color={colors.orange}
                            style={{marginStart: 20}}
                          />
                        </View>
                      : null}
              </View>
              <Text style={[styles.iconName, {marginStart: 10}]}>
                {item.name}
              </Text>
            </View>
          )}
        />
        <Text style={[styles.title, {marginTop: 15}]}>Warmup</Text>
        <View style={styles.playView}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.playFlex}>
              <Text style={styles.txtCard}>Brisk Walking</Text>
              <Text style={styles.txtNum}>10 Reps</Text>
            </View>
            <TouchableOpacity
              style={{flex: 0.2, justifyContent: 'space-evenly'}}
            >
              <AntDesign
                name="playcircleo"
                size={20}
                color={colors.CommonColorBB}
                style={{alignSelf: 'center', marginHorizontal: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
              <MaterialCommunityIcons
                name="microsoft-xbox-controller-menu"
                size={24}
                color={colors.CommonColorBB}
                style={{alignSelf: 'flex-start'}}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Text style={[styles.title, {marginTop: 20}]}>Workouts</Text>
        <Text style={styles.circuitTxt}>Circuit 1</Text>
        <Text style={[styles.txt, {marginTop: 2}]}>
          Repeat 3 Times Then Rest for 30 Seconds
        </Text>
        <View style={styles.playView}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.playFlex}>
              <Text style={styles.txtCard}>Modified Burpees</Text>
              <Text style={styles.txtNum}>10 Reps</Text>
            </View>
            <TouchableOpacity
              style={{flex: 0.2, justifyContent: 'space-evenly'}}
            >
              <AntDesign
                name="playcircleo"
                size={20}
                color={colors.CommonColorBB}
                style={{alignSelf: 'center', marginHorizontal: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
              <MaterialCommunityIcons
                name="microsoft-xbox-controller-menu"
                size={24}
                color={colors.CommonColorBB}
                style={{alignSelf: 'flex-start'}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.playView}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.playFlex}>
              <Text style={styles.txtCard}>Modified Push ups</Text>
              <Text style={styles.txtNum}>10 Reps</Text>
            </View>
            <TouchableOpacity
              style={{flex: 0.2, justifyContent: 'space-evenly'}}
            >
              <AntDesign
                name="playcircleo"
                size={20}
                color={colors.CommonColorBB}
                style={{alignSelf: 'center', marginHorizontal: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
              <MaterialCommunityIcons
                name="microsoft-xbox-controller-menu"
                size={24}
                color={colors.CommonColorBB}
                style={{alignSelf: 'flex-start'}}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Text style={[styles.circuitTxt, {marginTop: 10}]}>Circuit 2</Text>
        <Text style={[styles.txt, {marginTop: 2}]}>
          Repeat 3 Times Then Rest for 30 Seconds
        </Text>
        <View style={styles.playView}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.playFlex}>
              <Text style={styles.txtCard}>Crunches</Text>
              <Text style={styles.txtNum}>10 Reps</Text>
            </View>
            <TouchableOpacity
              style={{flex: 0.2, justifyContent: 'space-evenly'}}
            >
              <AntDesign
                name="playcircleo"
                size={20}
                color={colors.CommonColorBB}
                style={{alignSelf: 'center', marginHorizontal: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
              <MaterialCommunityIcons
                name="microsoft-xbox-controller-menu"
                size={24}
                color={colors.CommonColorBB}
                style={{alignSelf: 'flex-start'}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.playView}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.playFlex}>
              <Text style={styles.txtCard}>Stretch</Text>
              <Text style={styles.txtNum}>40 Seconds</Text>
            </View>
            <TouchableOpacity
              style={{flex: 0.2, justifyContent: 'space-evenly'}}
            >
              <AntDesign
                name="playcircleo"
                size={20}
                color={colors.CommonColorBB}
                style={{alignSelf: 'center', marginHorizontal: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
              <MaterialCommunityIcons
                name="microsoft-xbox-controller-menu"
                size={24}
                color={colors.CommonColorBB}
                style={{alignSelf: 'flex-start'}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.btnView}>
          <Text style={styles.btnTxt}>Complete My Workout</Text>
        </TouchableOpacity>

      </SafeAreaView>
    </ScrollView>
  );
};

export default Cardio;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  bgView: {
    backgroundColor: colors.CommonColor,
  },
  txt: {
    color: colors.CommonColorBB,
    fontSize: 15,
  },
  heading: {
    color: colors.CommonColorBB,
    fontWeight: 'bold',
    fontSize: 22,
  },
  title: {
    color: colors.CommonColorBB,
    fontSize: 18,
    fontWeight: '700',
  },
  iconName: {
    color: colors.CommonColorBB,
    fontSize: 16,
  },
  circuitTxt: {
    color: colors.CommonColorBB,
    fontSize: 16,
    fontWeight: '500',
    marginTop: 10,
  },
  btnView: {
    padding: 14,
    width: 250,
    marginTop: 30,
    marginBottom: 30,
    borderRadius: 30,
    backgroundColor: colors.CommonColorBB,
    alignSelf: 'center',
  },
  btnTxt: {
    color: colors.CommonColor,
    alignSelf: 'center',
    fontSize: 15,
  },
  txtCard: {
    color: colors.CommonColorBB,
    fontSize: 15,
    fontWeight: '500',
  },
  txtNum: {
    color: colors.CommonColorBB,
    fontSize: 10,
  },
  playView: {
    padding: 10,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: '#FDF0ED',
  },
  playFlex: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  FlatList,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../styles/Colors';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons
  from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProgressCircle from 'react-native-progress-circle';

const Workout = ({navigation}) => {
  const listItem = [
    {
      Name: 'John Smith',
      programName: 'Program Name',
    },
    {
      Name: 'John Smith',
      programName: 'Program Name',
    },
    {
      Name: 'John Smith',
      programName: 'Program Name',
    },
    //   {
    //    Name: 'Nandhini',
    //    programName: 'Program Name' ,
    //  },
    //   {
    //    Name: 'Janani',
    //    programName: 'Program Name' ,
    //  },
  ];

  const [data, setData] = useState ([
    {
      title: 'Workout',
      percent: '0 %',
      type: 0,
      isClicked: false,
    },
    {
      title: 'Meal',
      percent: '0 /6',
      type: 1,
      isClicked: false,
    },
    {
      title: 'Habits',
      percent: 'Pending',
      type: 2,
      isClicked: false,
    },
    {
      title: 'Steps',
      percent: '0',
      type: 3,
      isClicked: false,
    },
    {
      title: 'Water',
      percent: '0 %',
      type: 4,
      isClicked: false,
    },
    {
      title: 'Progress',
      percent: 'Tracker',
      type: 5,
      isClicked: true,
    },
  ]);

  const ChangeColor = (item, index) => {
    let ch = data.map ((it, ind) => {
      var temp = Object.assign ({}, it);
      if (ind === index) {
        temp.isClicked = true;
        return temp;
      } else {
        temp.isClicked = false;
        return temp;
      }
    });
    setData (ch);
  };

  return (
    <ScrollView style={styles.container}>
      <SafeAreaView>
        <View style={{flex: 0.1, flexDirection: 'row'}}>
          <View style={{flex: 2, justifyContent: 'center'}}>
            <Entypo
              name="menu"
              size={30}
              color={colors.CommonColor}
              style={{alignSelf: 'flex-start', marginStart: 10}}
            />
          </View>
          <View
            style={{
              flex: 5.55,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Image
              source={require ('../../assets/Biglee.png')}
              style={{marginTop: 5}}
            />
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: colors.CommonColor,
              }}
            >
              Program name
            </Text>
          </View>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <FontAwesome
              name="comment"
              size={22}
              color={colors.CommonColor}
              style={{alignSelf: 'flex-start'}}
            />
          </View>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <FontAwesome
              name="bell"
              size={22}
              color={colors.CommonColor}
              style={{alignSelf: 'flex-start'}}
            />
          </View>
        </View>

        <View style={{marginTop: 40, marginHorizontal: 20}}>
          <ImageBackground
            source={require ('../../assets/WorkoutMan.png')}
            style={styles.bgImage}
            imageStyle={{borderRadius: 10}}
            resizeMode="cover"
          >
            <View
              style={{
                backgroundColor: 'rgba(52, 52, 52, 0.2)',
                height: 425,
                borderRadius: 10,
              }}
            >
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 0.45, flexDirection: 'row'}}>
                  <View style={{flex: 1}} />
                  <View style={{flex: 2, alignItems: 'center'}}>
                    <Text style={[styles.txtTitle, {marginTop: 10}]}>
                      TODAY
                    </Text>
                    <Text style={[styles.txt, {marginTop: 2}]}>
                      Week 1 - Day 3
                    </Text>
                  </View>
                  <View style={{flex: 1, justifyContent: 'center'}}>
                    <MaterialIcons
                      name="arrow-forward-ios"
                      size={20}
                      color={colors.CommonColor}
                      style={{alignSelf: 'flex-end', marginEnd: 10}}
                    />
                  </View>
                </View>

                <View style={{flex: 2, marginTop: 0}}>
                  <FlatList
                    data={data}
                    numColumns={2}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({item, index}) => (
                      <View>
                        <TouchableOpacity
                          onPress={() => ChangeColor (item, index)}
                          activeOpacity={0.7}
                        >
                          <View
                            style={[
                              styles.cardView,
                              {
                                backgroundColor: item.isClicked === true
                                  ? 'rgba(232,56,56, 0.50)'
                                  : 'rgba(28, 28, 28, 0.70)',
                              },
                            ]}
                          >
                            <View style={{flex: 1}}>
                              {item.type == 0
                                ? <View>
                                    <Ionicons
                                      name="md-barbell-sharp"
                                      size={50}
                                      color={colors.CommonColor}
                                      style={styles.rotateIcon}
                                    />
                                  </View>
                                : item.type == 1
                                    ? <View>
                                        <MaterialCommunityIcons
                                          name="silverware-fork-knife"
                                          size={50}
                                          color={colors.CommonColor}
                                          style={{alignSelf: 'center'}}
                                        />
                                      </View>
                                    : item.type == 2
                                        ? <View>
                                            <Fontisto
                                              name="person"
                                              size={50}
                                              color={colors.CommonColor}
                                              style={{alignSelf: 'center'}}
                                            />
                                          </View>
                                        : item.type == 3
                                            ? <View>
                                                <MaterialCommunityIcons
                                                  name="shoe-print"
                                                  size={50}
                                                  color={colors.CommonColor}
                                                  style={{alignSelf: 'center'}}
                                                />
                                              </View>
                                            : item.type == 4
                                                ? <View>
                                                    <MaterialCommunityIcons
                                                      name="cup-water"
                                                      size={50}
                                                      color={colors.CommonColor}
                                                      style={{
                                                        alignSelf: 'center',
                                                      }}
                                                    />
                                                  </View>
                                                : item.type == 5
                                                    ? <View>
                                                        <MaterialCommunityIcons
                                                          name="watch-variant"
                                                          size={50}
                                                          color={
                                                            colors.CommonColor
                                                          }
                                                          style={{
                                                            alignSelf: 'center',
                                                          }}
                                                        />
                                                      </View>
                                                    : null}
                            </View>
                            <View
                              style={{
                                flex: 1,
                                flexDirection: 'column',
                                marginHorizontal: 2,
                              }}
                            >
                              <Text style={styles.itemTxt}>{item.title}</Text>
                              <Text style={styles.itemTxt}>{item.percent}</Text>
                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>
              </View>

            </View>
          </ImageBackground>
        </View>

        <View style={styles.performerView}>
          <View style={{flex: 0.25}}>
            <Text
              style={[
                styles.txtTitle,
                {marginTop: 20, alignSelf: 'center', textAlign: 'center'},
              ]}
            >
              Top 3{'\n'}Performers of the Week
            </Text>
          </View>
          <View style={{marginBottom: 20}}>
            <FlatList
              data={listItem}
              showsHorizontalScrollIndicator={false}
              renderItem={({item}) => (
                <View style={styles.reward}>
                  <View style={{flex: 1}}>
                    <FontAwesome5
                      name="trophy"
                      size={40}
                      color={colors.CommonColor}
                      style={{alignSelf: 'center'}}
                    />
                  </View>
                  <View style={{flex: 2, alignItems: 'center'}}>
                    <Text style={styles.Name}>{item.Name}</Text>
                    <Text style={styles.programName}>{item.programName}</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Image
                      source={require ('../../assets/Icon.png')}
                      style={styles.rewardIcon}
                    />
                  </View>
                </View>
              )}
            />
          </View>
        </View>
        <View style={styles.weeklyView}>
          <View style={{flex: 0.2}}>
            <Text
              style={[
                styles.txtTitle,
                {marginTop: 20, alignSelf: 'center', textAlign: 'center'},
              ]}
            >
              Weekly Stats
            </Text>
            <Text
              style={[
                styles.txt,
                {marginTop: 2, alignSelf: 'center', textAlign: 'center'},
              ]}
            >
              Week 4
            </Text>
          </View>

          <View style={{flex: 1.15, flexDirection: 'column'}}>
            <View style={{flex: 0.90, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={styles.meal}>Workout</Text>
                <View style={{alignSelf: 'center', marginTop: 15}}>
                  <ProgressCircle
                    percent={80}
                    radius={40}
                    borderWidth={8}
                    color={colors.background}
                    shadowColor={colors.CommonColor}
                    bgColor={colors.CommonColorBB}
                  >
                    <Text style={{fontSize: 20, color: colors.CommonColor}}>
                      5/7
                    </Text>
                  </ProgressCircle>
                </View>
                <Text style={[styles.txt2, {marginTop: 10}]}>
                  5 days Completed
                </Text>
                <Text style={styles.txt2}>1 day Pending</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.meal}>Meal</Text>
                <View style={{alignSelf: 'center', marginTop: 15}}>
                  <ProgressCircle
                    percent={70}
                    radius={40}
                    borderWidth={8}
                    color={colors.background}
                    shadowColor={colors.CommonColor}
                    bgColor={colors.CommonColorBB}
                  >
                    <Text style={{fontSize: 20, color: colors.CommonColor}}>
                      70%
                    </Text>
                  </ProgressCircle>
                </View>
                <Text style={[styles.txt2, {marginTop: 10}]}>
                  5 days Completed
                </Text>
                <Text style={styles.txt2}>1 day Pending</Text>
              </View>
            </View>
            <View style={{flex: 0.90, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={styles.meal}>Habit</Text>
                <View style={{alignSelf: 'center', marginTop: 15}}>
                  <ProgressCircle
                    percent={80}
                    radius={40}
                    borderWidth={8}
                    color={colors.background}
                    shadowColor={colors.CommonColor}
                    bgColor={colors.CommonColorBB}
                  >
                    <Text style={{fontSize: 20, color: colors.CommonColor}}>
                      5/7
                    </Text>
                  </ProgressCircle>
                </View>
                <Text style={[styles.txt2, {marginTop: 10}]}>
                  5 days Completed
                </Text>
                <Text style={styles.txt2}>1 day Pending</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.meal}>Steps</Text>
                <View style={{alignSelf: 'center', marginTop: 15}}>
                  <ProgressCircle
                    percent={80}
                    radius={40}
                    borderWidth={8}
                    color={colors.background}
                    shadowColor={colors.CommonColor}
                    bgColor={colors.CommonColorBB}
                  >
                    <Text style={{fontSize: 20, color: colors.CommonColor}}>
                      0
                    </Text>
                  </ProgressCircle>
                </View>
                <Text style={[styles.txt2, {marginTop: 10}]}>
                  Goal: 70k Steps
                </Text>
                <Text style={styles.txt2}>10k Per day</Text>
              </View>
            </View>
            <View style={{flex: 0.90, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={styles.meal}>Water Intake</Text>
                <View style={{alignSelf: 'center', marginTop: 15}}>
                  <ProgressCircle
                    percent={80}
                    radius={40}
                    borderWidth={8}
                    color={colors.background}
                    shadowColor={colors.CommonColor}
                    bgColor={colors.CommonColorBB}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: colors.CommonColor,
                        alignSelf: 'center',
                      }}
                    >
                      {'  '}0.00 {'\n'}{'    '}Its
                    </Text>
                  </ProgressCircle>
                </View>
                <Text style={[styles.txt2, {marginTop: 10}]}>
                  Goal: 21 Its per week
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: '500',
                    color: colors.CommonColor,
                    alignSelf: 'center',
                  }}
                />
                <Ionicons
                  name="ios-calendar"
                  size={80}
                  color={colors.CommonColor}
                  style={{alignSelf: 'center', marginTop: 10}}
                />
                <TouchableOpacity onPress={()=> navigation.navigate('Cardio')}>
                  <Text style={styles.Dashboard}>Progress Dashboard</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </View>

      </SafeAreaView>
    </ScrollView>
  );
};

export default Workout;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: colors.CommonColorBB,
  },
  bgImage: {
    alignSelf: 'stretch',
    height: 425,
    width: 320,
  },
  cardView: {
    flex: 0.55,
    flexDirection: 'row',
    marginTop: 10,
    height: 90,
    width: 140,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: colors.CommonColor,
    borderRadius: 5,
    alignItems: 'center',
    margin: 5,
  },
  rotateIcon: {
    paddingHorizontal: 8,
    transform: [{rotate: '140deg'}],
  },
  txtTitle: {
    fontSize: 20,
    fontWeight: '700',
    color: colors.CommonColor,
  },
  txt: {
    fontSize: 14,
    fontWeight: '300',
    color: colors.CommonColor,
  },
  txt2: {
    fontSize: 12,
    fontWeight: '300',
    color: colors.CommonColor,
    alignSelf: 'center',
  },
  meal: {
    fontSize: 16,
    fontWeight: '400',
    color: colors.CommonColor,
    alignSelf: 'center',
  },
  Dashboard: {
    fontSize: 12,
    fontWeight: '700',
    color: colors.CommonColor,
    alignSelf: 'center',
    marginTop: 10,
    textDecorationLine: 'underline',
  },
  performerView: {
    alignSelf: 'center',
    marginTop: 30,
    // height: 400,
    width: 320,
    shadowColor: colors.CommonColorBB,
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 5,
    borderWidth: 0.4,
    borderColor: colors.CommonColor,
    borderRadius: 10,
  },
  reward: {
    //flex: 0.22,
    flexDirection: 'row',
    marginTop: 20,
    height: 70,
    width: 250,
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: 'rgba(52, 52, 52, 0.99)',
    alignItems: 'center',
    margin: 5,
  },
  Name: {
    fontSize: 18,
    fontWeight: '500',
    color: colors.CommonColor,
  },
  programName: {
    fontSize: 14,
    fontWeight: '400',
    color: colors.CommonColor,
  },
  rewardIcon: {
    alignSelf: 'flex-end',
    marginHorizontal: 5,
    marginTop: -38,
  },
  weeklyView: {
    alignSelf: 'center',
    marginTop: 30,
    height: 700,
    width: 320,
    shadowColor: colors.CommonColorBB,
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 5,
    borderWidth: 0.4,
    borderColor: colors.CommonColor,
    borderRadius: 10,
    marginBottom: 30,
  },
  itemTxt: {
    fontSize: 16,
    fontWeight: '400',
    color: colors.CommonColor,
  },
});

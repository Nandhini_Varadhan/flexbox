import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootStackScreen from '../router/RootStackScreen';

const AppRouter = () => {

    return(

        <NavigationContainer>
            <RootStackScreen/>
        </NavigationContainer>
    );
};

export default AppRouter;
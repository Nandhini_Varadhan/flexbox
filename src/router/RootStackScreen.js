import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Workout from '../pages/Workout';
import Cardio from '../pages/Cardio';

const RootStack = createNativeStackNavigator ();

const RootStackScreen = () => (
    <RootStack.Navigator screenOptions ={{headerShown: false}}>
        <RootStack.Screen name ="Workout" component={Workout}/>
        <RootStack.Screen name ="Cardio" component = {Cardio}/>
    </RootStack.Navigator>
);

export default RootStackScreen;
const colors = {
  PrimaryBGColor: '#0095C6',
  SecondaryColor: '#CACACA',
  TertiaryColor: '#F9FAFC',
  CommonColor: '#FFFFFF',
  CommonColorBB: '#000000',
  SlectableColor:'#F5B5A3',
  LightColor:'#ECECEC',
  MaterialBlack:'#393939',
  ButtonColor:'#2DC7EE',
  BlueColor:'#2DC7EE',
  TabFocusColor:'#F5B5A3',
  icon:'#969696',
  transparentblue_color:'#2DC7EE1A',
  transparent_color:'#0000001A',
  grey_color:'#8C8C95',
  light_blacK:'rgba(0, 0, 0, 0.5)',
  chat_template:'rgba(199, 167, 134, 0.7)',
  orange:'#F07957',
  background: '#E83838',

};
export {colors};

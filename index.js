/**
 * @format
 */

import {AppRegistry, AppState} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Workout from './src/pages/Workout';
import AppRouter from './src/router/AppRouter';
import TextComponent from './src/pages/TextComponent';


AppRegistry.registerComponent(appName, () => TextComponent);
